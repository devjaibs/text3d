import { TextGeometry } from 'three/examples/jsm/geometries/TextGeometry';
import { FontLoader } from 'three/examples/jsm/loaders/fontLoader';
import { Group, MeshBasicMaterial, Mesh } from 'three';
import { RoundedBoxGeometry } from 'three/examples/jsm/geometries/RoundedBoxGeometry';
import interactionManager from './interactionManager';

function createBox(width = 1, height = 0.5, depth = 0.2, color = 0xf9f9f9) {
  const boxGeometry = new RoundedBoxGeometry(width, height, depth);
  const boxMaterial = new MeshBasicMaterial({
    color,
  });
  return new Mesh(boxGeometry, boxMaterial);
}

const fontLoader = new FontLoader();
async function createText(text, textHeight = 0.2, textColor = 0xc94c9c) {
  try {
    const font = await fontLoader.loadAsync(
      '/static/fonts/helvetiker_regular.typeface.json'
    );
    const textGeometry = new TextGeometry(text, {
      font,
      size: 0.12,
      height: textHeight,
    });

    const textMaterial = new MeshBasicMaterial({ color: textColor });
    const text3d = new Mesh(textGeometry, textMaterial);
    text3d.translateY(-0.05);

    return text3d;
  } catch (ex) {
    console.log(ex.message);
  }
}

let group = new Group();

async function createButton(
  text,
  { buttonHeight, buttonDepth, buttonColor, textHeight, textColor } = {}
) {
  const box = createBox(
    text.length * 0.1,
    buttonHeight,
    buttonDepth,
    buttonColor
  );
  const text3d = await createText(text, textHeight, textColor);
  text3d.translateX(-text.length * 0.035);

  group.add(box, text3d);

  group.addEventListener('mousedown', e => {
    e.target.scale.set(0.95, 0.95, 0.95);
  });

  group.addEventListener('click', e => {
    e.target.scale.set(1.0, 1.0, 1.0);
  });

  group.addEventListener('mouseover', e => {
    document.body.style.cursor = 'pointer';
  });

  group.addEventListener('mouseout', e => {
    document.body.style.cursor = 'default';
  });

  interactionManager.add(group);

  return group;
}

export { createText, createBox };
export default createButton;
