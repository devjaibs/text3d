import { Scene, CubeTextureLoader } from 'three';
import px from './img/px.png';
import py from './img/py.png';
import pz from './img/pz.png';
import nx from './img/nx.png';
import ny from './img/ny.png';
import nz from './img/nz.png';

const scene = new Scene();

const images = [px, nx, py, ny, pz, nz];

const loader = new CubeTextureLoader();
const texture = loader.load(images);

scene.background = texture;

export default scene;
