import './style.css';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import scene from './scene';
import camera from './camera';
import createButton from './button';
import sizes from './sizes';
import renderer from './renderer';
import interactionManager from './interactionManager';

window.addEventListener('resize', () => {
  sizes.setWidth(window.innerWidth);
  sizes.setHeight(window.innerHeight);

  camera.aspect = sizes.width / sizes.height;
  camera.updateProjectionMatrix();

  renderer.setSize(sizes.width, sizes.height);
  renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
});

const controls = new OrbitControls(camera, renderer.domElement);
controls.enableDamping = true;

function tick() {
  requestAnimationFrame(tick);
  renderer.render(scene, camera);
  controls.update();
  interactionManager.update();
}

tick();

createButton('Clickable!').then(button => scene.add(button));
