const sizes = {
  width: window.innerWidth,
  height: window.innerHeight,
  setWidth(width) {
    this.width = width;
  },
  setHeight(height) {
    this.height = height;
  },
};

export default sizes;
