import * as THREE from 'three';
import sizes from './sizes';

const camera = new THREE.PerspectiveCamera(45, sizes.width / sizes.height);
camera.position.setZ(5);

export default camera;
